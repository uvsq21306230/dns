package DNS.app;

import static org.junit.Assert.*;

import org.junit.Test;

public class RechercheNomTest {

	@Test
	public void test() {
		AdresseIP ip = new AdresseIP("172.168.60.2");
		NomMachine ma = new NomMachine("nom.test.fr");
		DnsItem it = new DnsItem(ip,ma);
		Dns dns = new Dns(it);
		RechercheNom i= new RechercheNom("172.168.60.2",dns);
		System.out.println(i.execute());
		assertEquals("Nom pour IP : 172.168.60.2 est : nom.test.fr",i.execute());
	}

}
