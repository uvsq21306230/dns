package DNS.app;

import static org.junit.Assert.*;

import org.junit.Test;

public class NomMachineTest {

	@Test
	public void testConstructeur() {
		NomMachine m = new NomMachine("machine.test.fr");
		assertEquals(m.getNomQualifieMachine(),"machine.test.fr");
	}

}
