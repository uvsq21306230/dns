package DNS.app;

import static org.junit.Assert.*;

import org.junit.Test;

public class AdresseIPTest {

	@Test
	public void testConstructeur() {
		AdresseIP ip = new AdresseIP("192.172.65.1");
		assertEquals(ip.getAddrIp(),"192.172.65.1");
	}

}
