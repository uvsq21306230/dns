package DNS.app;

import static org.junit.Assert.*;

import org.junit.Test;

public class DnsItemTest {

	@Test
	public void testConstructeur() {
		NomMachine m = new NomMachine("machine.test.fr");			
		AdresseIP ip = new AdresseIP("192.172.65.1");
			
		DnsItem dns = new DnsItem(ip,m);
		
		assertEquals(dns.getName(),"machine.test.fr");
		assertEquals(dns.getIp(),"192.172.65.1");
	}

}
