package DNS.app;

import static org.junit.Assert.*;

import org.junit.Test;

public class RechercheIPTest {

	@Test
	public void test() {
		AdresseIP ip = new AdresseIP("172.168.60.2");
		NomMachine ma = new NomMachine("nom.test.fr");
		DnsItem it = new DnsItem(ip,ma);
		Dns dns = new Dns(it);
		RechercheIP i= new RechercheIP("nom.test.fr",dns);
		System.out.println(i.execute());
		assertEquals("IP pour le PC : nom.test.fr est : 172.168.60.2",i.execute());
	}

}
