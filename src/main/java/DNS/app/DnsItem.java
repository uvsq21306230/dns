package DNS.app;

public class DnsItem {

	private AdresseIP ip;
	private NomMachine name;
	
	DnsItem(AdresseIP ip, NomMachine name) {
		this.ip = ip;
		this.name = name;
	}
	public String getIp() {
		return ip.getAddrIp();
	}
	public void setIp(AdresseIP ip) {
		this.ip = ip;
	}
	public String getName() {
		return name.getNomQualifieMachine();
	}
	public void setName(NomMachine name) {
		this.name = name;
	}
	public String toString() {
		return "DnsItem [ip=" + ip + ", name=" + name + "]";
	}
}
