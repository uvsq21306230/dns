package DNS.app;

public class RechercheIP implements Commande{

	private String Nom;
	private Dns dns;
	
	public RechercheIP(String Nom, Dns dns)
	{
		this.Nom = Nom;
		this.dns = dns;
	}
	
	
	public String execute(){
		if(dns.getItemNomMachine(Nom) != null)
		{
			return "IP pour le PC : " + Nom + " est : " + dns.getItemNomMachine(Nom).getIp();
		}
		else return "PC inconnu";
	}
		
}
