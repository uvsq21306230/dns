package DNS.app;

public class RechercheNom implements Commande{

	private String IP;
	private Dns dns;
	
	public RechercheNom(String IP, Dns dns)
	{
		this.IP = IP;
		this.dns = dns;
	}
	
	
	public String execute(){
		
		if(dns.getItemIP(IP)!= null)
		return "Nom pour IP : " + IP + " est : " + dns.getItemIP(IP).getName();
		else return "IP inconnue";
		}
}
