package DNS.app;


public class DnsTUI {
	
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    if(Integer.parseInt(str)<256)
	    return true;
	    return false;
	    
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	}
	
	public Commande NextCommande(Dns dns,String input)
	{
		String[] parts = input.split(" ");
		String[] parts2 = input.split("\\.");
			if(parts[0].equals("ls") && !parts[1].equals("-a"))
			{
				RechercheDomaine rd = new RechercheDomaine(parts[1],dns,0); //recherche nom donc mode == 0
				return rd;
			}
			
			else if(parts[0].equals("ls") && parts[1].equals("-a"))
			{
				RechercheDomaine rd = new RechercheDomaine(parts[2],dns,1);
				return rd;
			}
			
			else if(parts2.length==4 && (isNumeric(parts2[0])==true && isNumeric(parts2[1])==true && isNumeric(parts2[2])==true && isNumeric(parts2[3])==true))
			{
				RechercheNom rn = new RechercheNom(input,dns);
				return rn;
			}
			
			else if(input.equals("exit"))
			{
				quitter q = new quitter();
				return q;
			
			}
			else
			{
				RechercheIP ri = new RechercheIP(input,dns);
				return ri;
			}
		
	}
	
	public void affiche(String aff)
	{
		System.out.println(aff);	
	}
}
