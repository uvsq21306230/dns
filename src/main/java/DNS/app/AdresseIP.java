package DNS.app;

public class AdresseIP {

	private String addrIp;

	AdresseIP(String addrIp) {
		this.addrIp = addrIp;
	}
	
	public String getAddrIp() {
		return addrIp;
	}

	public void setAddrIp(String addrIp) {
		this.addrIp = addrIp;
	}
	
	public String toString() {
		return "AdresseIP [addrIp=" + addrIp + "]";
	}
	
}
