package DNS.app;

import java.util.Vector;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Dns {
	private Vector<DnsItem> DnsItemV;
	public Dns(DnsItem d){
		this.DnsItemV = new Vector<DnsItem>();
		this.DnsItemV.add(d);
	}
	
	public Dns(){
		this.DnsItemV = new Vector<DnsItem>();
		BufferedReader f = null;
		
	
		try {
			f = new BufferedReader(new FileReader("file.txt"));
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		String line = new String();
		try {
	
			String[] parts = line.split(" ");
			while((line = f.readLine()) != null) {
				
				parts = line.split(" ");
				
				DnsItemV.add((new DnsItem(new AdresseIP(parts[0]),new NomMachine(parts[1]))));
				
			}
		} catch (IOException e) {
		
			e.printStackTrace();
		}
	
		try {
			f.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		//done
	}
	public DnsItem getItemIP(String IP)
	{
		int i=0;
		//boucle dans DnsItemV ou IP = IP d'un pc dans la liste
			for(i=0;i<DnsItemV.size();i++){
				if(DnsItemV.elementAt(i).getIp().equals(IP ))
				{
					return DnsItemV.elementAt(i);
				}
			}	
		return null;
	}
	public DnsItem getItemNomMachine(String NomMachine)
	{
		int i=0;
		//boucle dans DnsItemV ou nomMachine = NomMachine d'un pc dans la liste
			for(i=0;i<DnsItemV.size();i++){
				if(DnsItemV.elementAt(i).getName().equals(NomMachine ))
				{
					return DnsItemV.elementAt(i);
				}
			}
		return null;
	}
	
	public Vector<DnsItem> getItemDomaine(String Domaine)
	{
		Vector<DnsItem> Res = new Vector<DnsItem>();
		try {
			int i = 0;
			String[] parts =  null;
			String s = new String();
			for(i = 0; i<DnsItemV.size();i++)
			{
				parts = DnsItemV.elementAt(i).getName().split("\\.",2);
				s = parts[1];
				if(s.equals(Domaine))
				{
					Res.add(DnsItemV.elementAt(i));
				}
			}
		}	
		catch(IndexOutOfBoundsException e)
		{
			e.printStackTrace();
		}
		
		return Res;
	}

}
