package DNS.app;

public class NomMachine {
	private String nomQualifieMachine;
	
	NomMachine(String nomQualifieMachine)
	{
		this.nomQualifieMachine = nomQualifieMachine;
	}

	public String toString() {
		return "NomMachine [nomQualifieMachine=" + nomQualifieMachine + "]";
	}

	public String getNomQualifieMachine() {
		return nomQualifieMachine;
	}

	public void setNomQualifieMachine(String nomQualifieMachine) {
		this.nomQualifieMachine = nomQualifieMachine;
	}
}
