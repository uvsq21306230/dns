package DNS.app;

import java.util.Scanner;

public class DnsApp{

	public static void main(String[] Args){
		DnsApp App = new DnsApp();
		App.run();
	}
	public void run(){
		DnsTUI ui = new DnsTUI();
		Dns d = new Dns();
		Commande c = null;
		
		Scanner sc = new Scanner(System.in);
		String input = sc.nextLine();
		boolean b = true;
		while(b)
		{
			try
			{
				c = ui.NextCommande(d,input);
				ui.affiche(c.execute());
			}catch(ArrayIndexOutOfBoundsException e)
			{
				System.out.println("Commande non valide \n");
			}
			input = sc.nextLine();
		}
			
		sc.close();
	}
}
