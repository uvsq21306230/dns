package DNS.app;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class RechercheDomaine implements Commande{

	private String Domaine;
	private Dns dns;
	private int mode;
	
	public RechercheDomaine(String Domaine, Dns dns, int mode)
	{
		this.Domaine = Domaine;
		this.dns = dns;
		this.mode = mode;
	}
	
	public String execute(){
		Vector<DnsItem> tmp = new Vector<DnsItem>();
		List<String> list = new ArrayList<String>();
		tmp = dns.getItemDomaine(Domaine);
		String s = new String();

		int i;
		if(!tmp.isEmpty())
		{
			if(mode == 1){ //option -a
				for(i = 0; i<tmp.size();i++)
				{
					list.add(i, tmp.elementAt(i).getIp()); 
				}
				Collections.sort(list);
				for(i = 0; i<list.size();i++)
				{
					s += "IP n : "+ (i+1) + " -> " + list.get(i) + "\n";
				}
				return s;
			}
			else{
				for(i = 0; i<tmp.size();i++)
				{
					list.add(i, tmp.elementAt(i).getName()); 
				}
				Collections.sort(list);
				for(i = 0; i<list.size();i++)
				{
					s += "Nom n : "+ (i+1) + " -> " + list.get(i) + "\n";
				}
				return s;
			}
		}
		return "Domaine inconnu";
	}
}
